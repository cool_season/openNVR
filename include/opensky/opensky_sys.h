#ifndef _OPEN_SKY_SYS_H_
#define _OPEN_SKY_SYS_H_

#include "opensky_type.h"

#ifdef __cplusplus
extern "C"{
#endif

int32_t sky_add_user(char *name, char *paaswd);

int32_t sky_mod_user(char *name, char *paaswd);

int32_t sky_del_user(char *name);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif