#ifndef _OPEN_SKY_NET_H_
#define _OPEN_SKY_NET_H_

#include "opensky_type.h"

#ifdef __cplusplus
extern "C"{
#endif

int32_t sky_set_rtsp_port(int32_t prot);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif