#ifndef _OPEN_SKY_CHANNEL_H_
#define _OPEN_SKY_CHANNEL_H_

#include "opensky_type.h"

#ifdef __cplusplus
extern "C"{
#endif

int32_t sky_add_channel(int32_t ch, const char *chInfo);

int32_t sky_mod_channel(int32_t ch, const char *chInfo);

int32_t sky_del_channel(int32_t ch);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif