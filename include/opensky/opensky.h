#ifndef _OPEN_SKY_H_
#define _OPEN_SKY_H_

#include "opensky_type.h"
#include "opensky_channel.h"
#include "opensky_net.h"
#include "opensky_sys.h"

#ifdef __cplusplus
extern "C"{
#endif

int32_t sky_init();

int32_t sky_start();

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif