#include <stdio.h>
#include <thread>
#include <mutex>
#include <iostream>
#include <condition_variable>

#include "opensky/opensky.h"

#include "nlohmann/json.hpp"

int32_t main(int32_t argc, char *argv[])
{
    int32_t iRet = sky_init();
    if (iRet != 0) {
        printf("sky_init failed \n");
        return -1;
    }

    iRet = sky_start();
    if (iRet != 0) {
        printf("sky_start failed \n");
        return -1;
    }

    // 添加Onvif设备
    nlohmann::json onvifInfo = {{"address", "192.168.146.152"}, {"username", "admin"}, {"password", "admin"},
                                {"protocol", "onvif"},          {"httpport", 12580},   {"channel", 0}};
    sky_add_channel(0, onvifInfo.dump().c_str());

    // 添加Rtsp设备
    nlohmann::json rtspInfo = {{"username", "admin"},
                               {"password", "abcd1234"},
                               {"protocol", "rtsp"},
                               {"mainurl", "rtsp://192.168.146.195:554/Streaming/Channels/101?transportmode=unicast"}};
    sky_add_channel(1, rtspInfo.dump().c_str());

    // 添加gb28181设备
    nlohmann::json gbInfo = {
        {"address", "192.168.146.152"}, {"username", "admin"}, {"password", "admin"},
        {"protocol", "gb28181"},        {"channel", 0},        {"regid", {{"ID", "34020000001180000001"}}}};
    sky_add_channel(2, gbInfo.dump().c_str());

    // 阻塞
    std::mutex m;
    std::condition_variable cv;
    std::unique_lock<std::mutex> lk(m);
    cv.wait(lk);

    return 0;
}