# 说明
面向摄像头的流媒体低代码开发平台。摄像头接入（rtsp、gb28181、onvif），流媒体分发（rtsp）。本库是开发demo，头文件位于include/OpenSky,lib为库。

# 编译
gcc需要高于4.8，已验证环境centos7
进入build，编译x86版本
cmake .. -DPLATFORM=x86

# 环境运行说明
设置动态库路径，再运行sky命令
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./lib/
./sky

# rtsp取流
示例如下：
rtsp://admin:admin@172.16.111.128:554/streaming/live?ch=1&type=0
其中IP替换为运行服务器IP

# 规划
1、支持录像存储，优先支持mp4存储。
2、支持智能分析，提供YUV数据，供上层分析。

# 架构说明
[架构说明](docs/架构.md)
