#!/bin/bash

BASE_DIR=$(dirname $(readlink -f $0))
buildPath=$BASE_DIR/build

if [ -d "$buildPath" ]; then
	rm -r "$buildPath"	
fi
mkdir "$buildPath"

cd $buildPath
cmake -DPLATFORM=m64x86 $BASE_DIR
cmake --build . -j
